public class Converter {
//Your names go here:
/*
* Sunny Nagam
* Keenan gaudio
* Connor Britton
*/
	private double celsiusToFahrenheit(double C){
		return (C*9/5) + 32;
	}
	
	private double fahrenheitToCelsius(double F){
		return (F - 32) * 5/9; 
	}
	
	public static void main(String[] args) {
	System.out.println(celsiusToFahrenheit(180));
	System.out.println(fahrenheitToCelsius(250));
    } 
}